package com.example.bookproject.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookproject.exceptions.ResourceNotFoundException;
import com.example.bookproject.modelDTO.BookDTO;
import com.example.bookproject.models.Book;
import com.example.bookproject.repositories.BookRepository;

@RestController
@RequestMapping("/api")
public class BookController {
	@Autowired
	BookRepository bookRepository;
	
	@GetMapping("/books/read")
	public HashMap<String, Object> getAllBookDTO() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Book> listBookEntity = (ArrayList<Book>) bookRepository.findAll();
		ArrayList<BookDTO> listBookDTO = new ArrayList<BookDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(Book book : listBookEntity) {
			BookDTO bookDTO = modelMapper.map(book, BookDTO.class);
			
			listBookDTO.add(bookDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Book's Data Success");
		result.put("Data", listBookDTO);
		
		return result;
	}
	
	public BigDecimal calculateBookPrice(Long publisherId, Long authorId) {
		BigDecimal paperPrice = new BigDecimal(0);
		BigDecimal ratePrice = new BigDecimal(0);
		BigDecimal rateBookPrice = new BigDecimal(1.1);
		
		ArrayList<Book> listBookEntity = (ArrayList<Book>) bookRepository.findAll();
		
		for(Book book : listBookEntity) {
			if((book.getPublisherId().getPublisherId() == publisherId) && (book.getAuthorId().getAuthorId() == authorId)) {
				paperPrice = book.getPublisherId().getPaperQuality().getPaperPrice();
				ratePrice = book.getAuthorId().getRatingId().getRatePrice();
				
			}
		}
		
		BigDecimal bookPrice = paperPrice.multiply(ratePrice).multiply(rateBookPrice);
		
		return bookPrice;
	}
	
	@PostMapping("/books/create")
	public HashMap<String, Object> createBookDTO(@Valid @RequestBody BookDTO bookDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Book bookEntity = modelMapper.map(bookDTO, Book.class);
		
		bookEntity.setPrice(calculateBookPrice(bookEntity.getPublisherId().getPublisherId(), bookEntity.getAuthorId().getAuthorId()));
		
		bookRepository.save(bookEntity);
		
		bookDTO.setBookId(bookEntity.getBookId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Book Success");
		result.put("Data", bookDTO);
		
		return result;
	}
	
	@PutMapping("/books/update/{id}")
	public HashMap<String, Object> updateBookDTO(@PathVariable(value = "id") Long bookId, @Valid @RequestBody BookDTO bookDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Book bookEntity = bookRepository.findById(bookId).orElseThrow(() -> new ResourceNotFoundException("Book", "id", bookId));
		bookEntity = modelMapper.map(bookDetails, Book.class);
		
		bookEntity.setBookId(bookId);
		
	    bookRepository.save(bookEntity);
	    
	    bookDetails = modelMapper.map(bookEntity, BookDTO.class);
	    
		result.put("Status", 200);
		result.put("Message", "Update Book Success");
		result.put("Data", bookDetails);
		
		return result;
	}
	
	@PutMapping("/books/update_price/{id}")
	public HashMap<String, Object> updateBookPrice(@PathVariable(value = "id") Long bookId, @Valid @RequestBody BookDTO bookDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Book bookEntity = bookRepository.findById(bookId).orElseThrow(() -> new ResourceNotFoundException("Book", "id", bookId));
		bookEntity = modelMapper.map(bookDetails, Book.class);
		
		bookEntity.setBookId(bookId);
		bookEntity.setPrice(calculateBookPrice(bookEntity.getPublisherId().getPublisherId(), bookEntity.getAuthorId().getAuthorId()));
		
	    bookRepository.save(bookEntity);
	    
	    bookDetails = modelMapper.map(bookEntity, BookDTO.class);
	    
		result.put("Status", 200);
		result.put("Message", "Update Book Success");
		result.put("Data", bookDetails);
		
		return result;
	}
	
	@DeleteMapping("/books/delete/{id}")
	public HashMap<String, Object> deleteBookDTO(@PathVariable(value = "id") Long bookId) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Book bookEntity = bookRepository.findById(bookId).orElseThrow(() -> new ResourceNotFoundException("Book", "id", bookId));
		BookDTO bookDTO = modelMapper.map(bookEntity, BookDTO.class);
		
		bookRepository.delete(bookEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Book Success");
		result.put("Data", bookDTO);
		
		return result;
	}
}