package com.example.bookproject.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookproject.exceptions.ResourceNotFoundException;
import com.example.bookproject.modelDTO.PublisherDTO;
import com.example.bookproject.models.Publisher;
import com.example.bookproject.repositories.PublisherRepository;

@RestController
@RequestMapping("/api")
public class PublisherController {
	@Autowired
	PublisherRepository publisherRepository;
	
	@GetMapping("/publishers/read")
	public HashMap<String, Object> getAllPublisherDTO() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Publisher> listPublisherEntity = (ArrayList<Publisher>) publisherRepository.findAll();
		ArrayList<PublisherDTO> listPublisherDTO = new ArrayList<PublisherDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(Publisher publisher : listPublisherEntity) {
			PublisherDTO publisherDTO = modelMapper.map(publisher, PublisherDTO.class);
			
			listPublisherDTO.add(publisherDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Publisher's Data Success");
		result.put("Data", listPublisherDTO);
		
		return result;
	}
	
	@PostMapping("/publishers/create")
	public HashMap<String, Object> createPublisherDTO(@Valid @RequestBody PublisherDTO publisherDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Publisher publisherEntity = modelMapper.map(publisherDTO, Publisher.class);;
		
		publisherRepository.save(publisherEntity);
		
		publisherDTO.setPublisherId(publisherEntity.getPublisherId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Publisher Success");
		result.put("Data", publisherDTO);
		
		return result;
	}
	
	@PutMapping("/publishers/update/{id}")
	public HashMap<String, Object> updatePublisherDTO(@PathVariable(value = "id") Long publisherId, @Valid @RequestBody PublisherDTO publisherDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Publisher publisherEntity = publisherRepository.findById(publisherId).orElseThrow(() -> new ResourceNotFoundException("Publisher", "id", publisherId));
		publisherEntity = modelMapper.map(publisherDetails, Publisher.class);
		
		publisherEntity.setPublisherId(publisherId);
		
	    publisherRepository.save(publisherEntity);
	    
	    publisherDetails = modelMapper.map(publisherEntity, PublisherDTO.class);
	    
		result.put("Status", 200);
		result.put("Message", "Update Publisher Success");
		result.put("Data", publisherDetails);
		
		return result;
	}
	
	@DeleteMapping("/publishers/delete/{id}")
	public HashMap<String, Object> deletePublisherDTO(@PathVariable(value = "id") Long publisherId) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Publisher publisherEntity = publisherRepository.findById(publisherId).orElseThrow(() -> new ResourceNotFoundException("Publisher", "id", publisherId));
		PublisherDTO publisherDTO = modelMapper.map(publisherEntity, PublisherDTO.class);
		
		publisherRepository.delete(publisherEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Publisher Success");
		result.put("Data", publisherDTO);
		
		return result;
	}
	
}