package com.example.bookproject.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookproject.exceptions.ResourceNotFoundException;
import com.example.bookproject.modelDTO.RatingDTO;
import com.example.bookproject.models.Rating;
import com.example.bookproject.repositories.RatingRepository;

@RestController
@RequestMapping("/api")
public class RatingController {
	@Autowired
	RatingRepository ratingRepository;
	
	@GetMapping("/ratings/read")
	public HashMap<String, Object> getAllRatingDTO() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Rating> listRatingEntity = (ArrayList<Rating>) ratingRepository.findAll();
		ArrayList<RatingDTO> listRatingDTO = new ArrayList<RatingDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(Rating rating : listRatingEntity) {
			RatingDTO ratingDTO = modelMapper.map(rating, RatingDTO.class);
			
			listRatingDTO.add(ratingDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Rating's Data Success");
		result.put("Data", listRatingDTO);
		
		return result;
	}
	
	@PostMapping("/ratings/create")
	public HashMap<String, Object> createRatingDTO(@Valid @RequestBody RatingDTO ratingDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Rating ratingEntity = modelMapper.map(ratingDTO, Rating.class);		
		
		ratingRepository.save(ratingEntity);
		
		ratingDTO.setRatingId(ratingEntity.getRatingId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Rating Success");
		result.put("Data", ratingDTO);
		
		return result;
	}
	
	@PutMapping("/ratings/update/{id}")
	public HashMap<String, Object> updateRatingDTO(@PathVariable(value = "id") Long ratingId, @Valid @RequestBody RatingDTO ratingDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Rating ratingEntity = ratingRepository.findById(ratingId).orElseThrow(() -> new ResourceNotFoundException("Rating", "id", ratingId));
		ratingEntity = modelMapper.map(ratingDetails, Rating.class);
		
		ratingEntity.setRatingId(ratingId);

		ratingRepository.save(ratingEntity);
		
		ratingDetails = modelMapper.map(ratingEntity, RatingDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Rating Success");
		result.put("Data", ratingDetails);
		
		return result;
	}
	
	@DeleteMapping("/ratings/delete/{id}")
	public HashMap<String, Object> deleteRatingDTO(@PathVariable(value = "id") Long ratingId) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Rating ratingEntity = ratingRepository.findById(ratingId).orElseThrow(() -> new ResourceNotFoundException("Rating", "id", ratingId));
		RatingDTO ratingDTO = modelMapper.map(ratingEntity, RatingDTO.class);
		
		ratingRepository.delete(ratingEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Rating Success");
		result.put("Data", ratingDTO);
		
		return result;
	}
}
