package com.example.bookproject.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookproject.exceptions.ResourceNotFoundException;
import com.example.bookproject.modelDTO.AuthorDTO;
import com.example.bookproject.models.Author;
import com.example.bookproject.repositories.AuthorRepository;

@RestController
@RequestMapping("/api")
public class AuthorController {
	@Autowired
	AuthorRepository authorRepository;
	
	@GetMapping("/authors/read")
	public HashMap<String, Object> getAllAuthorDTO() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Author> listAuthorEntity = (ArrayList<Author>) authorRepository.findAll();
		ArrayList<AuthorDTO> listAuthorDTO = new ArrayList<AuthorDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(Author author : listAuthorEntity) {
			AuthorDTO authorDTO = modelMapper.map(author, AuthorDTO.class);
			
			listAuthorDTO.add(authorDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Author's Data Success");
		result.put("Data", listAuthorDTO);
		
		return result;
	}
	
	@PostMapping("/authors/create")
	public HashMap<String, Object> createAuthorDTO(@Valid @RequestBody AuthorDTO authorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Author authorEntity = modelMapper.map(authorDTO, Author.class);;
		
		authorRepository.save(authorEntity);
		
		authorDTO.setAuthorId(authorEntity.getAuthorId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Author Success");
		result.put("Data", authorDTO);
		
		return result;
	}
	
	@PutMapping("/authors/update/{id}")
	public HashMap<String, Object> updateAuthorDTO(@PathVariable(value = "id") Long authorId, @Valid @RequestBody AuthorDTO authorDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Author authorEntity = authorRepository.findById(authorId).orElseThrow(() -> new ResourceNotFoundException("Author", "id", authorId));
		authorEntity = modelMapper.map(authorDetails, Author.class);
		
		authorEntity.setAuthorId(authorId);

		authorRepository.save(authorEntity);
		
		authorDetails = modelMapper.map(authorEntity, AuthorDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Author Success");
		result.put("Data", authorDetails);
		
		return result;
	}
	
	@DeleteMapping("/authors/delete/{id}")
	public HashMap<String, Object> deleteAuthorDTO(@PathVariable(value = "id") Long authorId) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Author authorEntity = authorRepository.findById(authorId).orElseThrow(() -> new ResourceNotFoundException("Author", "id", authorId));
		AuthorDTO authorDTO = modelMapper.map(authorEntity, AuthorDTO.class);
		
		authorRepository.delete(authorEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Author Success");
		result.put("Data", authorDTO);
		
		return result;
	}

}