package com.example.bookproject.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookproject.exceptions.ResourceNotFoundException;
import com.example.bookproject.modelDTO.PaperDTO;
import com.example.bookproject.models.Paper;
import com.example.bookproject.repositories.PaperRepository;

@RestController
@RequestMapping("/api")
public class PaperController {
	@Autowired
	PaperRepository paperRepository;

	@GetMapping("/paper_quality/read")
	public HashMap<String, Object> getAllPaperDTO() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Paper> listPaperEntity = (ArrayList<Paper>) paperRepository.findAll();
		ArrayList<PaperDTO> listPaperDTO = new ArrayList<PaperDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(Paper paper : listPaperEntity) {
			PaperDTO paperDTO = modelMapper.map(paper, PaperDTO.class);
			
			listPaperDTO.add(paperDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Paper's Data Success");
		result.put("Data", listPaperDTO);
		
		return result;
	}
	
	@PostMapping("/paper_quality/create")
	public HashMap<String, Object> createPaperDTO(@Valid @RequestBody PaperDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Paper paperEntity = modelMapper.map(body, Paper.class);		
		
		paperRepository.save(paperEntity);
		
		body.setPaperId(paperEntity.getPaperId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Paper Success");
		result.put("Data", body);
		
		return result;
	}
	
	@PutMapping("/paper_quality/update/{id}")
	public HashMap<String, Object> updatePaperDTO(@PathVariable(value = "id") Long paperId, @Valid @RequestBody PaperDTO paperDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Paper paperEntity = paperRepository.findById(paperId).orElseThrow(() -> new ResourceNotFoundException("PaperQuality", "id", paperId));
		paperEntity = modelMapper.map(paperDetails, Paper.class);
		
		paperEntity.setPaperId(paperId);

		paperRepository.save(paperEntity);
		
		paperDetails = modelMapper.map(paperEntity, PaperDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Paper Success");
		result.put("Data", paperDetails);
		
		return result;
	}
	
	@DeleteMapping("/paper_quality/delete/{id}")
	public HashMap<String, Object> deletePaperDTO(@PathVariable(value = "id") Long paperId) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Paper paperEntity = paperRepository.findById(paperId).orElseThrow(() -> new ResourceNotFoundException("PaperQuality", "id", paperId));
		PaperDTO paperDTO = modelMapper.map(paperEntity, PaperDTO.class);
		
		paperRepository.delete(paperEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Paper Success");
		result.put("Data", paperDTO);
		
		return result;
	}
	
}