package com.example.bookproject.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Positive;

@Entity(name = "Paper")
@Table(name = "paper_quality")
public class Paper implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "paper_id", nullable = false, unique = true)
	private Long paperId;
	
	@Column(name = "quality_name", nullable = false)
	private String qualityName;
	
	@Column(name = "paper_price", nullable = false)
	@Positive
	private BigDecimal paperPrice;
	
	@OneToMany(
			mappedBy = "paperQuality",
			cascade = CascadeType.PERSIST,
			fetch = FetchType.LAZY
			)
	
	private Set<Publisher> publishers;
	
	public Paper() {
		publishers = new HashSet<Publisher>();
	}
	
	public Paper(String qualityName, BigDecimal paperPrice) {
		this.qualityName = qualityName;
		this.paperPrice = paperPrice;
		publishers = new HashSet<Publisher>();
	}

	public Long getPaperId() {
		return paperId;
	}
	
	public void setPaperId(Long paperId) {
		this.paperId = paperId;
	}
	
	public String getQualityName() {
		return qualityName;
	}
	
	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}
	
	public BigDecimal getPaperPrice() {
		return paperPrice;
	}
	
	public void setPaperPrice(BigDecimal paperPrice) {
		this.paperPrice = paperPrice;
	}

	public Set<Publisher> getPublishers() {
		return publishers;
	}

	public void setPublishers(Set<Publisher> publishers) {
		this.publishers = publishers;
		for(Publisher publisher : publishers) {
			publisher.setPaperQuality(this);
		}
	}
	
}
