package com.example.bookproject.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Positive;

@Entity
@Table(name = "ratings")
public class Rating implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "rating_id", nullable = false, unique = true)
	private Long ratingId;
	
	@Column(name = "rating", nullable = false)
	private String rating;
	
	@Column(name = "rate_price", nullable = false)
	@Positive
	private BigDecimal ratePrice;
	
	@OneToMany(
			mappedBy = "ratingId",
			cascade = CascadeType.PERSIST,
			fetch = FetchType.LAZY)
	private Set<Author> authors;

	public Rating() {
		authors = new HashSet<Author>();
	}

	public Rating(Long ratingId, String rating, BigDecimal ratePrice) {
		super();
		this.ratingId = ratingId;
		this.rating = rating;
		this.ratePrice = ratePrice;
		authors = new HashSet<Author>();
	}

	public Long getRatingId() {
		return ratingId;
	}

	public void setRatingId(Long ratingId) {
		this.ratingId = ratingId;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public BigDecimal getRatePrice() {
		return ratePrice;
	}

	public void setRatePrice(BigDecimal ratePrice) {
		this.ratePrice = ratePrice;
	}

	public Set<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
		for(Author author : authors) {
			author.setRatingId(this);
		}
	}
	
}
