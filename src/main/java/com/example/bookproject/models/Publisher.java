package com.example.bookproject.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "publishers")
public class Publisher implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "publisher_id", nullable = false, unique = true)
	private Long publisherId;
	
	@Column(name = "company_name", nullable = false)
	private String companyName;
	
	@Column(name = "country", nullable = false)
	private String country;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "paperId")
	private Paper paperQuality;
	
	@OneToMany(
			mappedBy = "publisherId",
			cascade = CascadeType.PERSIST,
			fetch = FetchType.LAZY)
	private Set<Book> books;
	
	public Publisher() {
		books = new HashSet<Book>();
	}
	
	public Publisher(String companyName, String country) {
		this.companyName = companyName;
		this.country = country;
		books = new HashSet<Book>();
	}

	public Long getPublisherId() {
		return publisherId;
	}
	
	public void setPublisherId(Long publisherId) {
		this.publisherId = publisherId;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}

	public Paper getPaperQuality() {
		return paperQuality;
	}

	public void setPaperQuality(Paper paperQuality) {
		this.paperQuality = paperQuality;
		paperQuality.getPublishers().add(this);
	}

	public Set<Book> getBooks() {
		return books;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
		for(Book book : books) {
			book.setPublisherId(this);
		}
	}
	
}
