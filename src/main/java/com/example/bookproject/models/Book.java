package com.example.bookproject.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.PositiveOrZero;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity(name = "Book")
@Table(name = "books")
public class Book implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "book_id", nullable = false, unique = true)
	private Long bookId;
	
	@Column(name = "title", nullable = false)
	private String title;
	
	@Column(name = "release_date", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date releaseDate;
	
	@Column(name = "price", nullable = false)
	@PositiveOrZero
	private BigDecimal price;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "publisherId")
	private Publisher publisherId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "authorId")
	private Author authorId;
	
	public Long getBookId() {
		return bookId;
	}
	
	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public Date getReleaseDate() {
		return releaseDate;
	}
	
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	
	public BigDecimal getPrice() {
		return price;
	}
	
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	public Publisher getPublisherId() {
		return publisherId;
	}
	
	public void setPublisherId(Publisher publisherId) {
		this.publisherId = publisherId;
		publisherId.getBooks().add(this);
	}
	
	public Author getAuthorId() {
		return authorId;
	}
	
	public void setAuthorId(Author authorId) {
		this.authorId = authorId;
		authorId.getBooks().add(this);
	}
	
}
