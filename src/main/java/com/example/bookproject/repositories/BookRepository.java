package com.example.bookproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.bookproject.models.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

}
