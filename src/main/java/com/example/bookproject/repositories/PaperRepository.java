package com.example.bookproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.bookproject.models.Paper;

@Repository
public interface PaperRepository extends JpaRepository<Paper, Long> {

}
