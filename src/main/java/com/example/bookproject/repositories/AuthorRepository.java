package com.example.bookproject.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.bookproject.models.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {

}
