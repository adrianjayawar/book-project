package com.example.bookproject.modelDTO;

public class AuthorDTO {
	private Long authorId;
	private String firstName;
	private String lastName;
	private String gender;
	private int age;
	private String country;
	private RatingDTO ratingId;
	
	public AuthorDTO() {
		
	}

	public AuthorDTO(Long authorId, String firstName, String lastName, String gender, int age, String country, RatingDTO ratingId) {
		this.authorId = authorId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.age = age;
		this.country = country;
		this.ratingId = ratingId;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public RatingDTO getRatingId() {
		return ratingId;
	}

	public void setRatingId(RatingDTO ratingId) {
		this.ratingId = ratingId;
	}
	
}
