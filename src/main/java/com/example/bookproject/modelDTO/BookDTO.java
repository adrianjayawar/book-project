package com.example.bookproject.modelDTO;

import java.math.BigDecimal;
import java.util.Date;

public class BookDTO {
	private Long bookId;
	private String title;
	private Date releaseDate;
	private BigDecimal price;
	private PublisherDTO publisherId;
	private AuthorDTO authorId;
	
	public BookDTO() {
		
	}

	public BookDTO(Long bookId, String title, Date releaseDate, BigDecimal price, PublisherDTO publisherId, AuthorDTO authorId) {
		this.bookId = bookId;
		this.title = title;
		this.releaseDate = releaseDate;
		this.price = price;
		this.publisherId = publisherId;
		this.authorId = authorId;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public PublisherDTO getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(PublisherDTO publisherId) {
		this.publisherId = publisherId;
	}

	public AuthorDTO getAuthorId() {
		return authorId;
	}

	public void setAuthorId(AuthorDTO authorId) {
		this.authorId = authorId;
	}
	
}
