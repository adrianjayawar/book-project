package com.example.bookproject.modelDTO;

import java.math.BigDecimal;

public class RatingDTO {
	private Long ratingId;
	private String rating;
	private BigDecimal ratePrice;
	
	public RatingDTO() {

	}

	public RatingDTO(Long ratingId, String rating, BigDecimal ratePrice) {
		this.ratingId = ratingId;
		this.rating = rating;
		this.ratePrice = ratePrice;
	}

	public Long getRatingId() {
		return ratingId;
	}

	public void setRatingId(Long ratingId) {
		this.ratingId = ratingId;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public BigDecimal getRatePrice() {
		return ratePrice;
	}

	public void setRatePrice(BigDecimal ratePrice) {
		this.ratePrice = ratePrice;
	}
	
}
