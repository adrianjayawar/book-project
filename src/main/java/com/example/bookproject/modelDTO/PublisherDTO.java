package com.example.bookproject.modelDTO;

public class PublisherDTO {
	private Long publisherId;
	private String companyName;
	private String country;
	private PaperDTO paperQuality;
	
	public PublisherDTO() {
		
	}

	public PublisherDTO(Long publisherId, String companyName, String country, PaperDTO paperQuality) {
		this.publisherId = publisherId;
		this.companyName = companyName;
		this.country = country;
		this.paperQuality = paperQuality;
	}

	public Long getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(Long publisherId) {
		this.publisherId = publisherId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public PaperDTO getPaperQuality() {
		return paperQuality;
	}

	public void setPaperQuality(PaperDTO paperQuality) {
		this.paperQuality = paperQuality;
	}
	
}
